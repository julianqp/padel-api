import mongoose, { Schema } from "mongoose"
import autoIncrement from "mongoose-auto-increment"
autoIncrement.initialize(mongoose.connection)

const Player = new Schema({
  id: {
    type: ID,
  },
  user: {
    type: String,
  },
})

/**
 * DOCUMENTACION: EQUIPO
 * player1: informacion del primer jugador del equipo
 * player2: informacion del segundo jugador del equipo
 */
const Equipo = new Schema({
  player1: Player,
  player2: Player,
})

const Puntos = new Schema({
  player1: Player,
  player2: Player,
})

/**
 * DOCUMENTACION: PARTIDO
 *
 * id: identificador del partido visible para los usuarios
 * tipo: tipo de partido, individual o por parejas
 * equipo1: información del equipo 1
 * equipo1: información del equipo 1
 * puntos: información sobre los puntos
 * creado: fecha de creacion del partido
 */
const PartidoModel = new Schema({
  id: {
    type: Number,
    unique: true,
  },
  tipo: {
    type: String,
    enum: ["INDIVIDUAL", "DOBLES"],
    default: "DOBLES",
  },
  equipo1: {
    type: Equipo,
    required: true,
  },
  equipo2: {
    type: Equipo,
    required: true,
  },
  puntos: {
    type: [Puntos],
    trim: true,
  },
  creado: {
    type: Date,
    default: Date.now(),
  },
})

PartidoModel.plugin(autoIncrement.plugin, {
  model: "Partido",
  field: "id",
  startAt: 1,
  incrementBy: 1,
})

export default mongoose.model("Partido", PartidoModel)
