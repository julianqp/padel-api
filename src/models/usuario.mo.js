import mongoose, { Schema } from "mongoose"

/**
 * DOCUMENTACION: USUARIO
 *
 * user: identificador que hace referencia al usuario
 * nombre: nombre del usuario
 * apellidos: apellidos del usuario
 * emial: correo electronico del usaurio
 * pass: constrase de acceso a cuenta del usuario
 * creado: fecha de creacion del usuario
 */
const UsuarioModel = new Schema({
  user: {
    type: String,
    trim: true,
    unique: true,
    required: true,
  },
  nombre: {
    type: String,
    trim: true,
    required: true,
  },
  apellidos: {
    type: String,
    trim: true,
    required: true,
  },
  email: {
    type: String,
    trim: true,
    unique: true,
    required: true,
  },
  pass: {
    type: String,
    trim: true,
    required: true,
  },
  creado: {
    type: Date,
    default: Date.now(),
  },
})

export default mongoose.model("Usuario", UsuarioModel)
