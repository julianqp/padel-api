import { ApolloServer } from "apollo-server-express"
import cors from "cors"
import express from "express"
import typeDefs from "./schemas"
import resolvers from "./resolvers"
import connectDB from "./config/db"

connectDB()

const app = express()

app.use(cors())

const server = new ApolloServer({ typeDefs, resolvers })

server.applyMiddleware({ app, path: "/graphql" })

app.listen(4000, () => {
  console.log("http://localhost:4000/graphql")
})
