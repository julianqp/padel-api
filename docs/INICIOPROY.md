# Inicio de proyecto

Pasos seguidos para crear el proyecto.

- Crear repositorio en Gitlab
- `npm init` (Seguir los pasos)
- Creamos archivo `.gitignore` y añadimos el contenido que no queremos añadir a git
- `npm i dotenv mongoose express apollo-server-express cors graphql`
- `npm i -D nodemon @babel/core @babel/node @babel/preset-env`
- Creamos el archivo `.babelrc` y añadimos la configuración
- Creamos carpeta `src`
- Creamos archivo `index.js` dentro de `src`
- Craremos las carpertas `resolvers` y `schemas` dentro de `src` con un archivo `index.js`
- `npm run dev` para comprobar si se inicia correctamente

Ya tenemos la estructura básica de un proyecto

Ahora prepararemos la conexion con la base de datos.
En mi caso usaré una basa de datos no relacional, mongoDB. Adicinalmente usararé moongoose como ORM para gestionar con modelos las tablas de la base de datos.

Para guardar los datos sensible de la conexión usaremos varibales de entorno, para tener mayor seguridad y no exponer los datos.

- Crear carpeta `congig` dentro de `src` y dentro un srchivo `bd.js`
- Añadir en `ìndex.js` la función para instanciar la conexión.
