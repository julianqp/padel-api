# Modelos

Para crear la estructura de las tablas de base de datos usaremos modelos, con mongoose para construir esa estructura con sus tipos. Evitando así que se añadan a la base de datos de mongo datos incorrectamente.

Los modelos serán la capa intemedia entre los funciones y la base de datos y nos ayudará a realizar las operaciones CRUD sobre la bd.

Para ellos crearemos una carpeta `models` dentro de `src`.

Se seguirá una estructura para nombrar los archivos, que será la siguiente:

    {nombre}.mo.js

El nombre será siempre en singular y siguiendo la estructura de camelcase.

Ejemplo:

    usuario.mo.js
    archivoComprimido.mo.js

Se deberá comentar las varibles con una pequeña defición para que cualquier persona que entre en el proyecto pueda saber a que hace referencia.
